<h1>Your crowdmatching dashboard

<h2>Summary

<table.summary>
  <tr>
    <td colspan=2>
      Balance not yet charged:
    <td>
      #{pendingDonation}
  <tr>
    <td colspan=2>
      Total donated to date:
    <td>
      #{totalDonation}
  <tr>
    <td colspan=2>
      Total Stripe fees to date:
    <td>
      #{totalFee}
  <tr>
    <td colspan=2>
      Total charged to date:
    <td>
      #{totalCharged}

<h3>Alpha note
<p>
  In October 2017, this alpha site began running crowdmatch events for
  <a href=@{SnowdriftProjectR}>Snowdrift.coop itself
  as our first test project. We began processing actual donations in 2023.
  Your support helps us to continue developing and testing the upcoming beta
  site which will support other FLO projects as well. Thank you for helping us
  clear the path! Besides participating in crowdmatching, you can help us
  further by
  <a href=@{DonateR}>donating larger start-up funding
  and volunteering.
  <a href=@{ContactR}>Contact&nbsp;us
  to learn how you can help.


<h2>Pledges
$maybe _ <- patronPledgeSince patron
  <p>
    You are currently pledged to 1 project:
  <p>
    • <a href=@{SnowdriftProjectR}>Snowdrift.coop</a>,
    matching a crowd of #{projectCrowd project} patrons.
    The current pledge value is #{projectPledgeValue project}.
$nothing
  <.notpledged>
    <p>
      You are currently not pledged to any project.
    $maybe _ <- patronPaymentToken patron
      Join the
      <a href=@{SnowdriftProjectR}>Snowdrift.coop
      crowdmatching now!
    $nothing
      <p>
        Before pledging, you must configure payment settings through
        Stripe (see below).


<h2>Budget
<.sitelimit>
  <p>
    You will <em>never</em> be charged more than the budget limit of $10 per
    month.
  <p>


<h2>Payment method
$maybe _ <- patronPaymentToken patron
  <p>
    Your payment method is Stripe.
    <a href=@{PaymentInfoR}>Configure your payment settings.
$nothing
  <.prop-js-note>
    <p>
      <strong>Sorry!
      The Payment Settings page relies on Stripe's proprietary JavaScript to maintain
      compliance in handling credit card information.
    <p>
      To avoid this, we would need to implement a method to take financial
      information and pass it, server-side, to Stripe's API without storing any
      secure information ourselves. When this alpha site was built, we lacked the
      resources to handle that adequately, so we have made this one compromise to our
      dedication otherwise to software freedom.
    <p>
      We will resolve this in our upcoming beta overhaul of the site.
    <a .bigbutton href=@{PaymentInfoR}>Continue to payment settings


<h2>Charge History
<p>
  To keep processing fees below 10%, we only charge your pending donations
  when the total gets beyond #{minimumDonation}. After each charge, your
  balance will reset and must reach the minimum again before the next charge.

<table.charge-history>
  $if null payouts
    <tr>
      <td colspan=4 align=center>
        <em>You have not been charged yet.
  $else
    <tr>
      <td>Date
      <td>Total Charged
      <td>Donation Amount
      <td>+ Stripe Fee
    $forall (chargeDate, total, amount, fee) <- payouts
      <tr>
        <td>#{chargeDate}
        <td>#{total}
        <td>#{amount}
        <td>#{fee}

<h2>Crowdmatch history
<p>
  Once pledged, we add a donation each month to your balance based on
  the pledge values at the time.

<table.crowdmatch-progress>
  $if null crowdmatches
    <tr>
      <td colspan=3 align=center>
        <em>You have not been part of a crowdmatch yet.
  $else
    <tr>
      <td>Project
      <td>Month
      <td>Donation Amount
    $forall month <- crowdmatches
      <tr>
        <td>
          <a href=@{SnowdriftProjectR}>
            Snowdrift.coop
        <td>#{fst month}
        <td>#{snd month}
