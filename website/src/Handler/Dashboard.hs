module Handler.Dashboard (getDashboardR) where

import Import

import Text.Blaze.Html (ToMarkup(..))

import Crowdmatch
import Handler.TH
import MarkupInstances ()

getDashboardR :: Handler Html
getDashboardR = do
    Entity uid _ <- requireAuth
    (patron, project) <- runDB $ (,) <$> fetchPatron uid <*> fetchProject
    payouts <- runDB $ fetchPatronPayouts uid
    let crowdmatches = reverse $ map withMonthView (patronCrowdmatches patron)
        pendingDonation = patronDonationPayable patron
        totalDonation = foldr (+) 0 (map getDonation payouts)
        totalFee = foldr (+) 0 (map getFee payouts)
        totalCharged = foldr (+) 0 (map getCharge payouts)
    $(widget "page/dashboard" "Dashboard")
  where
    withMonthView (CrowdmatchDay d, amt) = (MonthView d, amt)
    getDonation :: PayoutInstance -> DonationUnits
    getDonation (_, _, donation, _) = donation
    getFee :: PayoutInstance -> Cents
    getFee (_, _, _, fee) = fee
    getCharge :: PayoutInstance -> Cents
    getCharge (_, charge, _, _) = charge

newtype MonthView = MonthView Day

instance ToMarkup MonthView where
    toMarkup (MonthView t) =
        toMarkup (formatTime defaultTimeLocale "%b %Y" t)
