#!/usr/bin/env bash

set -Eeuo pipefail

# Rebuilds the snowdrift-builder image based on the Dockerfile.

cd "$(dirname "$0")"
docker build  ../docker-images/image-build \
    --build-arg USER="$(id -un)" \
    --build-arg UID="$(id -u)" \
    --build-arg INSTALL_TOOLCHAIN=true \
    -t snowdrift-builder "$@"
