#!/usr/bin/env bash

set -Eeuo pipefail

# Just (re)starts an existing container based on the snowdrift-builder image.
# Build the image with rebuild.sh iff necessary (see warning there).

docker start -ai snowbuild
