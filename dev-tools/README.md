Steps to running build.sh in a Docker container:

## 1. Build your customized image

    ./docker-rebuild.sh

This creates a user in the image with the same uid as your actual user, to avoid
permissions problems.

## 2. Run it

    ./docker-rerun.sh

## 3. Use build.sh (inside the container)

    ./build.sh help

With the container, stack will see changes to your source code, so you should be
able to rebuild just by rerunning ./build.sh. The development site will be
available at https://localhost:3000 as usual.

If you have an old .stack-work or .postgres-work sitting around, you might need
to move it out of the way before starting the container.

## 4. Repeat the process

After the first run, the container is left sitting around, which is what we
want. To start it again, run

    ./docker-continue.sh
