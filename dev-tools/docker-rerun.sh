#!/usr/bin/env bash

set -Eeuo pipefail

# Recreates a container based on the snowdrift-builder image.
# WARNING: This removes the existing container, which can be costly. You'll have
# to rebuild all your Haskell dependencies.

code_repo="${1:-$(realpath "$(dirname "$0")"/..)}"

# Clean the old container. Prompt the user if they really want to proceed.
if [ -n "$(docker ps -a -q -f name=snowbuild)" ]; then
    read -p "This will remove the existing container. Are you sure you want to proceed? [y/N] " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        exit 0
    fi
    docker container rm snowbuild >/dev/null
fi

docker run -it --mount "type=bind,source=${code_repo},destination=/home/$USER/snowdrift" -p 3000:3000 --name snowbuild snowdrift-builder
