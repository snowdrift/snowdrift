#!/usr/bin/env bash
set -Eeuo pipefail

#
# ./dev-tools/psql.sh: Connects to the dev database with psql.
#

export PGHOST="${PWD}/.postgres-work"
export PGDATABASE=snowdrift_development

make -f db.makefile
psql
