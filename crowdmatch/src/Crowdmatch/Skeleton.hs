{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This is the closet, where we keep the skeletons.
--
-- Esqueleto produces the most wicked type errors when things go awry.
-- Spooky! But given the alternatives of using rawSql (and thus losing the
-- best of type safety when building sql), or using plain ol' persistent
-- and doing all the relational logic in Haskell (how depressing), I will
-- reluctantly stick with Esqueleto. Its use will merely be sequestered
-- in this module (alternate name: Model.Sequestro!)
module Crowdmatch.Skeleton where

import Control.Error hiding (isNothing, just)
import Control.Lens ((%~), _1, _2)
import Control.Monad.IO.Class (MonadIO)
import Data.Time (UTCTime)
import Database.Esqueleto.Legacy
import qualified Data.Text as T

import Crowdmatch.Model

{-# ANN module ("HLint: ignore Redundant bracket" :: String) #-}

-- | Retrieve the history of donations to the project
projectDonationHistory :: MonadIO m => SqlPersistT m [(HistoryTime, Int)]
projectDonationHistory =
    fmap (map ((_1 %~ unValue) . (_2 %~ fromMaybe 0 . unValue))) $
    select $
    from $ \dh -> do
        groupBy (time dh)
        orderBy [asc (time dh)]
        pure (time dh, total dh)
  where
    time = (^. DonationHistoryTime)
    total = sum_ . (^. DonationHistoryAmount)

newtype CheckTime = CheckTime { checkUTCTime :: UTCTime }
    deriving (Show)
newtype CrowdmatchTime = CrowdmatchTime { crowdUTCTime :: UTCTime }
    deriving (Show)

-- | Patrons actively pledged to Snowdrift since before a given time.
--
-- If someone drops their pledge after the crowdmatch time but before the check
-- time, they will not be considered active.
--
-- @@
--          v crowdmatch timestamp
--          |                    v check timestamp
-- ---------|--------------------|------------------->         time
--         x|xxxxxxxxxxxxxxxxxxxx|xxx---------------->         patron A, x = time they are pledged
-- @@
--
-- The @x@'s must fill the whole time range for patron A to be included in the crowdmatch.
--
-- To calculate this, we need to see that the last pledge action before the
-- check timestamp is a 'Create' that predates the crowdmatch timestamp.
--
activePatrons
    :: MonadIO m
    => CrowdmatchTime
    -> CheckTime
    -> SqlPersistT m (Maybe [Entity Patron])
activePatrons (CrowdmatchTime crowd) (CheckTime check)
    | crowd > check = pure Nothing
    | otherwise = Just <$> rawSql
        (T.unlines
            [ "with ranked_history as ("
            , "  select *,"
            , "    row_number() over ("
            , "      partition by patron"
            , "      order by time desc"
            , "    ) as rownum"
            , "  from crowdmatch__pledge_history"
            , "  where time < ?"
            , ")"
            , "select ??"
            , "from crowdmatch__patron"
            , "join ranked_history"
            , "on crowdmatch__patron.id = ranked_history.patron"
            , "where rownum = 1"
            , "and action = ?"
            , "and ranked_history.time < ?"
            ])
        [ toPersistValue check
        , toPersistValue Create
        , toPersistValue crowd
        ]

-- | Patrons with outstanding donation balances.
-- Guess that [Entity Patron] is a pair of (patronID, Patron)
patronsReceivable :: MonadIO m => DonationUnits -> SqlPersistT m [Entity Patron]
patronsReceivable minBal =
    rawSql
        (T.unlines
            [ "select ??"
            , "from crowdmatch__patron"
            , "where payment_token is not null"
            , "and donation_payable >= ?"
            ])
        [ toPersistValue minBal
        ]

-- | Charge a specfic person (Team Members, to start) with an outstanding donation balance.
--
-- It probably makes sense to drop the "minimum balance" requirement, since this
-- is an email that the user specifically entered to charge on the command line.
-- It is kept for now because there is not yet any confirmation presented to the
-- user before running the charge.
specificPatronReceivable :: MonadIO m => DonationUnits -> Int -> SqlPersistT m [Entity Patron]
specificPatronReceivable minBal usrId =
    rawSql
        (T.unlines
            [ "select ??"
            , "from crowdmatch__patron"
            , "where payment_token is not null"
            , "and donation_payable >= ?"
            , "and usr = ?"
            ])
        [ toPersistValue minBal
        , toPersistValue usrId
        ]

sumField
  :: ( PersistEntityBackend val ~ SqlBackend
     , PersistEntity val
     , PersistField a
     , Num a
     , MonadIO m
     )
     => EntityField val a
     -> SqlPersistT m a
sumField f = do
    rows <-
        select $
        from $ \entity ->
            return $ coalesceDefault [sum_ (entity ^. f)] $ val 0
    case rows of
        [] -> pure 0
        (Value x:_) -> pure x
