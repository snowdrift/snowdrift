Currently Docker is used for two things.

1. Running Snowdrift in CI on images with dependencies pre-installed, which
   makes us a little more independent of the CI platform being used.
2. Building Snowdrift on your own computer in case that is necessary (it
   normally shouldn't be).
